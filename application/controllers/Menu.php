<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

require APPPATH . '/libraries/REST_Controller.php';
 
class Menu extends CI_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model("Menu_model");
    }
 
    function get_data()
    {
        $limit = $this->input->get('limit');
        $start = $this->input->get('start');
        $q = $this->input->get('q');
        $menu = $this->Menu_model->get_limit_data($limit, $start, $q);
        echo json_encode($menu, JSON_PRETTY_PRINT);
    }

    function get_all()
    {
        $menu = $this->Menu_model->get_all();
        echo json_encode($menu, JSON_PRETTY_PRINT);
    }

    public function total_rows()
    {
        $cari = $this->input->get("q");
        $total = $this->Menu_model->total_rows($cari);
        echo json_encode($total, JSON_PRETTY_PRINT);
    }
 
    // insert new data to menu
    function insert() { 
        $data = array(
                    'title'         => $this->input->post('title'),
                    'icon'          => $this->input->post('icon'),
                    'url'           => $this->input->post('url'),
                    'is_main_menu'  => $this->input->post('is_main_menu'),
                    'is_aktif'      => $this->input->post('is_aktif')
                );
        $insert = $this->Menu_model->insert($data);
        if ($insert) {
            echo json_encode(array('status' => 'success', 'data' => $data, 200, JSON_PRETTY_PRINT));
        } else {
            echo json_encode(array('status' => 'fail', 502, JSON_PRETTY_PRINT));
        }
    }
 
    // update data menu
    function update() {
        $id_menu = $this->input->post('id_menu');
         $data = array(
                    'title'         => $this->input->post('title'),
                    'icon'          => $this->input->post('icon'),
                    'url'           => $this->input->post('url'),
                    'is_main_menu'  => $this->input->post('is_main_menu'),
                    'is_aktif'      => $this->input->post('is_aktif')
                );
        $update = $this->Menu_model->update($id_menu, $data);
        if ($update) {
            echo json_encode(array('status' => 'success', 'data' => $data, 200, JSON_PRETTY_PRINT));
        } else {
            echo json_encode(array('status' => 'fail', 502, JSON_PRETTY_PRINT));
        }
    }
 
    // delete menu
    function delete() {
        $id_menu = $this->input->post('id_menu');
        $delete = $this->Menu_model->delete($id_menu);
        if ($delete) {
            echo json_encode(array('status' => 'success', 200, JSON_PRETTY_PRINT));
        } else {
            echo json_encode(array('status' => 'fail', 502, JSON_PRETTY_PRINT));
        }
    }
}