<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

require APPPATH . '/libraries/REST_Controller.php';
 
class User extends REST_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model("User_model");
    }
 
    // show data menu
    function index_get() {
        $id_user = $this->get('id_user');
        if ($id_user == '') {
            $menu = $this->User_model->get_data(NULL);
        } else {
            $menu = $this->User_model->get_data($id_user);
        }
        $this->response($menu, 200);
    }
 
    // insert new data to menu
    function index_post() {
        $data = array(
                    'id_user'       => $this->post('id_user'),
                    'nama_lengkap'  => $this->post('nama_lengkap'),
                    'email'         => $this->post('email'),
                    'password'      => $this->post('password'),
                    'level'         => $this->post('level'),
                    'status'        => $this->post('status')
                );
        $insert = $this->User_model->insert($data);
        if ($insert) {
            $this->response(array('status' => 'success', 'data' => $data, 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // update data menu
    function index_put() {
        $id_user = $this->put('id_user');
       $data = array(
                    'nama_lengkap'  => $this->put('nama_lengkap'),
                    'email'         => $this->put('email'),
                    'password'      => $this->put('password'),
                    'level'         => $this->put('level'),
                    'status'        => $this->put('status')
                );
        $update = $this->User_model->update($id_user, $data);
        if ($update) {
            $this->response(array('status' => 'success', 'data' => $data, 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // delete menu
    function index_delete() {
        $id_user = $this->delete('id_user');
        $delete = $this->User_model->delete($id_user);
        if ($delete) {
            $this->response(array('status' => 'success'), 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
}