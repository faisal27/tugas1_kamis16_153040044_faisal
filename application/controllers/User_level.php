<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

require APPPATH . '/libraries/REST_Controller.php';
 
class User_level extends REST_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model("User_level_model");
    }
 
    // show data menu
    function index_get() {
        $id_level_user = $this->get('id_level_user');
        if ($id_level_user == '') {
            $menu = $this->User_level_model->get_data(NULL);
        } else {
            $menu = $this->User_level_model->get_data($id_level_user);
        }
        $this->response($menu, 200);
    }
 
    // insert new data to level
    function index_post() {
        $data = array(
                    'nama_level'  => $this->post('nama_level')
                );
        $insert = $this->User_level_model->insert($data);
        if ($insert) {
            $this->response(array('status' => 'success', 'data' => $data, 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // update data level
    function index_put() {
        $id_level_user = $this->put('id_level_user');
        $data = array(
                    'nama_level'  => $this->put('nama_level')
                );
        $update = $this->User_level_model->update($id_level_user, $data);
        if ($update) {
            $this->response(array('status' => 'success', 'data' => $data, 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // delete level
    function index_delete() {
        $id_level_user = $this->delete('id_level_user');
        $delete = $this->User_level_model->delete($id_level_user);
        if ($delete) {
            $this->response(array('status' => 'success'), 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}