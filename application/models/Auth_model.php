<?php

class Auth_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function checkUser($email, $password)
	{
		$this->db->select("*");
		$this->db->from("tbl_user");
		$this->db->where("email", $email);
		$this->db->where("password", $password);
		return $this->db->get();
	}

	public function checkEmail($email)
	{
		$this->db->where('email', $email);
		return $this->db->get('tbl_user');
	}

	public function get_by_cookie($cookie)
	{
		$this->db->where('cookie', $cookie);
		return $this->db->get('tbl_user');
	}

	public function update($data, $id_user)
	{
		$this->db->where('id_user', $id_user);
		$this->db->update('tbl_user', $data);
	}
}
