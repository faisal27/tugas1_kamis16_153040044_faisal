<?php

class Menu_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($id_menu = null){
		$this->db->select('*');
		$this->db->from('tbl_menu');

		if ($id_menu == null) {
			$this->db->order_by('id_menu', 'asc');
		}else {
			$this->db->where('id_menu', $id_menu);
		}

		return $this->db->get()->result();
	}

	public function get_all(){
		return $this->db->get('tbl_menu')->result();
	}

	function get_limit_data($limit = null, $start = null, $q = NULL) {
        $this->db->order_by("id_menu", "DESC");
		$this->db->or_like('title', $q);
		$this->db->or_like('icon', $q);
		$this->db->or_like('url', $q);
		$this->db->or_like('is_main_menu', $q);
		$this->db->or_like('is_aktif', $q);
		$this->db->limit($limit, $start);
        return $this->db->get("tbl_menu")->result();
    }

    public function total_rows($q = null)
    {
		$this->db->or_like('title', $q);
		$this->db->or_like('icon', $q);
		$this->db->or_like('url', $q);
		$this->db->or_like('is_main_menu', $q);
		$this->db->or_like('is_aktif', $q);
		$this->db->from("tbl_menu");
		return $this->db->count_all_results();
    }

    //fungsi insert ke database
    public function insert($data){
       $this->db->insert('tbl_menu', $data);
       return TRUE;
    }

    public function delete($var){
    	$this->db->where("id_menu", $var);
    	$this->db->delete("tbl_menu");
    	return TRUE;
	}

	public function update($id, $data) {
		$this->db->where('id_menu', $id);
		$this->db->update("tbl_menu", $data);
		return TRUE;
	}
}
