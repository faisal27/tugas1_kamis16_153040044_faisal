<?php

class User_level_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($id_level_user = null){
		$this->db->select('*');
		$this->db->from('tbl_level_user');

		if ($id_level_user == null) {
			$this->db->order_by('id_level_user', 'asc');
		}else {
			$this->db->where('id_level_user', $id_level_user);
		}

		return $this->db->get()->result();
	}

    //fungsi insert ke database
    public function insert($data){
       $this->db->insert('tbl_level_user', $data);
       return TRUE;
    }

    public function delete($var){
    	$this->db->where("id_level_user", $var);
    	$this->db->delete("tbl_level_user");
    	return TRUE;
	}

	public function update($id, $data) {
		$this->db->where('id_level_user', $id);
		$this->db->update("tbl_level_user", $data);
		return TRUE;
	}
}
